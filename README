The Sortix Operating System
===========================

Sortix is a small self-hosting Unix-like operating system developed since 2011
aiming to be a clean and modern POSIX implementation. There's a lot of technical
debt that needs to be paid, but it's getting better. Traditional design mistakes
are avoided or aggressively deprecated by updating the base system and ports as
needed. The Sortix kernel, standard libraries, and most utilities were written
entirely from scratch. The system is halfway through becoming multi-user and
while security vulnerabilities are recognized as bugs, it should be considered
insecure at this time.

Links
-----

For more information, documentation and news, please visit the official website:

  https://sortix.org/

You can also download the newest release and cutting edge nightly builds at:

  https://users-cs.au.dk/sortie/sortix/release/

You can retrieve the current git master from our project page at:

  https://gitlab.com/sortix/sortix

System Requirements
-------------------

Sortix has low system requirements. It also works well under virtual machines
such as VirtualBox and Qemu:

* A 32-bit x86 (with SSE) or 64-bit x86_64 CPU.
* A few dozen megabytes of RAM - or if you are using a cdrom release with ports
  then you likely need at least twice the size of the cdrom image.
* A harddisk or cdrom drive or support for booting from USB.
* A multiboot compliant bootloader if booting from harddisk.
* A Parallel ATA harddisk, if you wish to access it from Sortix. The AHCI driver
  has not been merged yet.

Documentation
-------------

You can find the documentation for Sortix use and development at your local
documentation mirror, for instance:

  The documentation directory in a Sortix system:
  /share/doc

  The doc directory inside the Sortix source repository:
  doc/

  The official Sortix website:
  https://sortix.org/doc/

Building Sortix
---------------

If you wish to build Sortix under Sortix itself, please consult the user guide:

  $MY_LOCAL_DOCUMENTATION_MIRROR/user-guide

If you wish to cross-build Sortix from another operating system, please read
the cross development guide:

  $MY_LOCAL_DOCUMENTATION_MIRROR/cross-development

Documentation
-------------

Basic usage of the system has been documented at:

  $MY_LOCAL_DOCUMENTATION_MIRROR/user-guide

License
-------

Copyright(C) Jonas 'Sortie' Termansen <sortie@maxsi.org> and contributors 2011,
2012, 2013, 2014.

Sortix is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the COPYING-GPL and COPYING-LGPL files for more
information.

See the individual files for copyright terms. If a file does not contain a
license header, you can assume it is released under the GNU General Public
Licenser, either version 3 or (at your option) any later version. This includes
Sortix-related experimental branches and repositories found on Gitlab: these
things are so experimental that I might not have added copyright statements.

The build scripts might not contain a copyright license in which case they are
covered by the standard license for the software component they relate to.

Unless the license header in the source code states otherwise, the Sortix
kernel, the filesystem servers, the initrd tools, the utilities, the games, the
benchmark programs, regression tests, base programs, editors, shell, init, and
the tix package management programs are licensed under the GNU General Public
License, either version 3 or (at your option) any later version.

Unless the license header in the source code states otherwise, the libc library,
the libpthread library, and the libdispd library are licensed under the GNU
Lesser General Public License, either version 3 or (at your option) any later
version.

The Sortix math library (libm) is licensed as described in the libm/LEGAL file.
